<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>sdk-qq</artifactId>
	<version>1.2.1</version>
	<packaging>pom</packaging>

	<name>${project.groupId}:${project.artifactId}</name>
	<description>qq component with spring boot</description>
	<url>https://gitee.com/guerlab_net/sdk-qq</url>
	
	<parent>
		<groupId>net.guerlab</groupId>
		<artifactId>sdk-dependencies</artifactId>
		<version>1.0.0</version>
	</parent>

	<licenses>
		<license>
			<name>The Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>guer</id>
			<name>guer</name>
			<email>master@guerlab.net</email>
			<organization>guerlab</organization>
			<organizationUrl>http://www.guerlab.net</organizationUrl>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:https://gitee.com/guerlab_net/sdk-qq.git</connection>
		<developerConnection>scm:git:https://gitee.com/guerlab_net/sdk-qq.git</developerConnection>
		<url>https://gitee.com/guerlab_net/sdk-qq</url>
		<tag>HEAD</tag>
	</scm>

	<properties>
		<qq.version>1.2.1</qq.version>
	</properties>

	<repositories>
		<repository>
			<id>guerlab</id>
			<url>http://maven.guerlab.net/repository/public/</url>
		</repository>
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>guerlab</id>
			<url>http://maven.guerlab.net/repository/public/</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<downloadUrl>https://gitee.com/guerlab_net</downloadUrl>
		<repository>
			<id>guerlab-release</id>
			<name>guerlab Release Repository</name>
			<url>http://maven.guerlab.net/repository/guerlab-release</url>
		</repository>
		<snapshotRepository>
			<id>guerlab-snapshot</id>
			<name>guerlab Snapshot Repository</name>
			<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
		</snapshotRepository>
	</distributionManagement>
	
	<profiles>
		<profile>
			<id>guerlab</id>
			<repositories>
				<repository>
					<id>guerlab-snapshot</id>
					<name>guerlab Snapshots</name>
					<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
				<repository>
					<id>guerlab-releases</id>
					<name>guerlab Releases</name>
					<url>http://maven.guerlab.net/repository/release</url>
					<snapshots>
						<enabled>false</enabled>
					</snapshots>
				</repository>
			</repositories>
			<pluginRepositories>
				<pluginRepository>
					<id>guerlab-snapshot</id>
					<name>guerlab Snapshots</name>
					<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</pluginRepository>
			</pluginRepositories>
		</profile>
		<profile>
			<id>central</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>${maven-gpg-plugin.version}</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
			<distributionManagement>
				<snapshotRepository>
					<id>sonatype-nexus-snapshots</id>
					<name>Sonatype Nexus Snapshots</name>
					<url>https://oss.sonatype.org/content/repositories/snapshots/</url>
				</snapshotRepository>
				<repository>
					<id>sonatype-nexus-staging</id>
					<name>Nexus Release Repository</name>
					<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
				</repository>
			</distributionManagement>
		</profile>
	</profiles>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<executions>
						<execution>
							<goals>
								<goal>integration-test</goal>
								<goal>verify</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>pl.project13.maven</groupId>
					<artifactId>git-commit-id-plugin</artifactId>
					<executions>
						<execution>
							<goals>
								<goal>revision</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<verbose>true</verbose>
						<dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
						<generateGitPropertiesFile>true</generateGitPropertiesFile>
						<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<configuration>
						<source>${java.version}</source>
						<target>${java.version}</target>
						<encoding>${project.encoding}</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<configuration>
						<encoding>${project.encoding}</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>${plugin.javadoc.version}</version>
					<configuration>
						<encoding>${project.encoding}</encoding>
						<aggregate>true</aggregate>
						<charset>${project.encoding}</charset>
						<docencoding>${project.encoding}</docencoding>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<modules>
		<module>sdk-qq-core</module>
		<module>sdk-qq-starter</module>
		<module>sdk-qq-autoconfigue</module>
	</modules>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>net.guerlab</groupId>
				<artifactId>sdk-qq-core</artifactId>
				<version>${qq.version}</version>
			</dependency>
			<dependency>
				<groupId>net.guerlab</groupId>
				<artifactId>sdk-qq-autoconfigue</artifactId>
				<version>${qq.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
</project>