package net.guerlab.sdk.qq.config;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.qq.client.QQClient;
import net.guerlab.sdk.qq.client.impl.DefaultQQClient;
import okhttp3.OkHttpClient;

@Configuration
@RefreshScope
@EnableConfigurationProperties(QQOauth2Config.class)
public class QQOauth2AutoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(QQOauth2AutoConfiguration.class);

    @Autowired
    private QQOauth2Config config;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired(required = false)
    private OkHttpClient okHttpClient;

    @RefreshScope
    @Bean
    public QQClient qqClient() {
        return new DefaultQQClient(config.getAppId(), config.getAppKey(), getOkHttpClient(), objectMapper);
    }

    private OkHttpClient getOkHttpClient() {
        if (okHttpClient != null) {
            return okHttpClient;
        }

        return createHttpClient();
    }

    /**
     * 创建http请求客户端
     *
     * @return http请求客户端
     */
    public static OkHttpClient createHttpClient() {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS).sslSocketFactory(createSSLSocketFactory(), new TrustAllManager())
                .hostnameVerifier((
                        hostname,
                        session) -> true)
                .build();
    }

    private static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory sSLSocketFactory = null;

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[] {
                    new TrustAllManager()
            }, new SecureRandom());
            sSLSocketFactory = sc.getSocketFactory();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }

        return sSLSocketFactory;
    }

    private static class TrustAllManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(
                X509Certificate[] chain,
                String authType) throws CertificateException {
            /**
             * not todo some thing
             */
        }

        @Override
        public void checkServerTrusted(
                X509Certificate[] chain,
                String authType) throws CertificateException {
            /**
             * not todo some thing
             */
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }
}
