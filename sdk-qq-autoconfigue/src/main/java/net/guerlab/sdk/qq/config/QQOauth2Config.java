package net.guerlab.sdk.qq.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * QQ互联配置
 *
 * @author Administrator
 *
 */
@Component
@RefreshScope
@ConfigurationProperties(prefix = QQOauth2Config.CONFIG_PREFIX)
public class QQOauth2Config {

    public static final String CONFIG_PREFIX = "sdk.qq";

    private String appId;

    private String appKey;

    private String redirectUri;

    private String scope;

    private Map<String, String> retrunUrls;

    public String getAppId() {
        return appId;
    }

    public void setAppId(
            String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(
            String appKey) {
        this.appKey = appKey;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(
            String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(
            String scope) {
        this.scope = scope;
    }

    public Map<String, String> getRetrunUrls() {
        return retrunUrls;
    }

    public void setRetrunUrls(
            Map<String, String> retrunUrls) {
        this.retrunUrls = retrunUrls;
    }

    public String getRetrunUrl(
            String type) {
        return retrunUrls == null ? null : retrunUrls.get(type);
    }
}
