package net.guerlab.sdk.qq.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 错误数据
 * 
 * @author guer
 *
 */
public class ErrorData {

    /**
     * 错误信息
     */
    private String error;

    /**
     * 错误说明
     */
    @JsonProperty("error_description")
    private String errorDescription;

    /**
     * 返回 错误信息
     *
     * @return 错误信息
     */
    public String getError() {
        return error;
    }

    /**
     * 设置错误信息
     *
     * @param error
     *            错误信息
     */
    public void setError(
            String error) {
        this.error = error;
    }

    /**
     * 返回 错误说明
     *
     * @return 错误说明
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * 设置错误说明
     *
     * @param errorDescription
     *            错误说明
     */
    public void setErrorDescription(
            String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
