package net.guerlab.sdk.qq.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessToken {

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("expires_in")
    private long expiresIn;

    @JsonProperty("refresh_token")
    private String refreshToken;

    /**
     * 返回 accessToken
     *
     * @return accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置accessToken
     *
     * @param accessToken
     *            accessToken
     */
    public void setAccessToken(
            String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 返回 expiresIn
     *
     * @return expiresIn
     */
    public long getExpiresIn() {
        return expiresIn;
    }

    /**
     * 设置expiresIn
     *
     * @param expiresIn
     *            expiresIn
     */
    public void setExpiresIn(
            long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * 返回 refreshToken
     *
     * @return refreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 设置refreshToken
     *
     * @param refreshToken
     *            refreshToken
     */
    public void setRefreshToken(
            String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccessToken [accessToken=");
        builder.append(accessToken);
        builder.append(", expiresIn=");
        builder.append(expiresIn);
        builder.append(", refreshToken=");
        builder.append(refreshToken);
        builder.append("]");
        return builder.toString();
    }
}
