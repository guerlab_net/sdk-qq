package net.guerlab.sdk.qq.client;

import net.guerlab.sdk.qq.request.AbstractRequest;
import net.guerlab.sdk.qq.response.AbstractResponse;

public interface QQClient {

    <T extends AbstractResponse<E>, E> T execute(
            AbstractRequest<T, E> oauth2QQRequest);
}
