package net.guerlab.sdk.qq.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.qq.QQOauth2Exception;
import net.guerlab.sdk.qq.client.AbstractQQClient;
import net.guerlab.sdk.qq.request.AbstractRequest;
import net.guerlab.sdk.qq.response.AbstractResponse;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DefaultQQClient extends AbstractQQClient {

    /**
     * http请求客户端
     */
    private OkHttpClient client;

    /**
     * objectMapper
     */
    private ObjectMapper objectMapper;

    public DefaultQQClient(
            String appId,
            String appKey,
            OkHttpClient client,
            ObjectMapper objectMapper) {
        super(appId, appKey);
        this.client = client;
        this.objectMapper = objectMapper;
    }

    @Override
    public <T extends AbstractResponse<E>, E> T execute(
            AbstractRequest<T, E> oauth2QQRequest) {

        oauth2QQRequest.setObjectMapper(objectMapper);

        String uri = oauth2QQRequest.getRequestUri(appId, appKey);

        if (!oauth2QQRequest.needHttpRequest()) {
            return oauth2QQRequest.execute(uri).getResponse();
        }

        Call call = client.newCall(new Request.Builder().url(uri).build());
        try {
            Response response = call.execute();
            return oauth2QQRequest.execute(response.body().string()).getResponse();
        } catch (QQOauth2Exception e) {
            throw e;
        } catch (Exception e) {
            throw new QQOauth2Exception(e.getMessage(), e);
        }
    }

    /**
     * 返回 http请求客户端
     *
     * @return http请求客户端
     */
    public OkHttpClient getClient() {
        return client;
    }

    /**
     * 设置http请求客户端
     *
     * @param client
     *            http请求客户端
     */
    public void setClient(
            OkHttpClient client) {
        this.client = client;
    }

    /**
     * 返回 objectMapper
     *
     * @return objectMapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * 设置objectMapper
     *
     * @param objectMapper
     *            objectMapper
     */
    public void setObjectMapper(
            ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
