package net.guerlab.sdk.qq.request;

import net.guerlab.sdk.qq.QQConstants;
import net.guerlab.sdk.qq.response.AuthorizationCodeResponse;

public class AuthorizationCodeRequest extends AbstractRequest<AuthorizationCodeResponse, String> {

    @Override
    public StringBuilder createRequestUrl(
            String appId,
            String appKey) {
        StringBuilder builder = new StringBuilder(QQConstants.AUTHORIZE_URL);
        builder.append("?response_type=code&client_id=");
        builder.append(appId);
        return builder;
    }

    @Override
    public boolean needHttpRequest() {
        return false;
    }

    @Override
    protected void execut0(
            String responseData) {
        response = new AuthorizationCodeResponse();
        response.setData(responseData);
    }

    public void setScope(
            String scope) {
        requestParams.put("scope", scope);
    }

    public String getScope() {
        return requestParams.get("scope");
    }

    public void setRedirectUri(
            String redirectUri) {
        requestParams.put("redirect_uri", redirectUri);
    }

    public String getRedirectUri() {
        return requestParams.get("redirect_uri");
    }

    public void setState(
            String state) {
        requestParams.put("state", state);
    }

    public String getState() {
        return requestParams.get("state");
    }
}
