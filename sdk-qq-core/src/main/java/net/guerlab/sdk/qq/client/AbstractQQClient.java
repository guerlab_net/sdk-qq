package net.guerlab.sdk.qq.client;

public abstract class AbstractQQClient implements QQClient {

    protected String appId;

    protected String appKey;

    public AbstractQQClient(
            String appId,
            String appKey) {
        this.appId = appId;
        this.appKey = appKey;
    }
}
