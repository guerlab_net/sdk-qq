package net.guerlab.sdk.qq.response;

public abstract class AbstractResponse<T> {

    private T data;

    public final T getData() {
        return data;
    }

    public final void setData(
            T data) {
        this.data = data;
    }
}
