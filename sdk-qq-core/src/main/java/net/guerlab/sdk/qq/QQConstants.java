package net.guerlab.sdk.qq;

public interface QQConstants {

    String DEFAULT_SCOPE = "get_user_info,add_topic,add_one_blog,add_album,upload_pic,list_album,add_share,check_page_fans,add_t,add_pic_t,del_t,get_repost_list,get_info,get_other_info,get_fanslist,get_idollist,add_idol,del_ido,get_tenpay_addr";

    String VERSION = "2.0.0.0";

    String BASE_URL = "https://graph.qq.com/";

    String AUTHORIZE_URL = BASE_URL + "oauth2.0/authorize";

    String ACCESS_TOKEN_URL = BASE_URL + "oauth2.0/token";

    String GET_USER_INFO_URL = BASE_URL + "user/get_user_info";

    String GET_OPENID_URL = BASE_URL + "oauth2.0/me";

    String CHARSET_UTF8 = "UTF-8";
}
