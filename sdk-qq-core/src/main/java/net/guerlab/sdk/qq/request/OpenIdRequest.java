package net.guerlab.sdk.qq.request;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.qq.QQConstants;
import net.guerlab.sdk.qq.QQOauth2Exception;
import net.guerlab.sdk.qq.response.OpenIdInfo;
import net.guerlab.sdk.qq.response.OpenIdResponse;
import net.guerlab.sdk.qq.util.URLUtil;

public class OpenIdRequest extends AbstractRequest<OpenIdResponse, OpenIdInfo> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpenIdRequest.class);

    @Override
    public StringBuilder createRequestUrl(
            String appId,
            String appKey) {
        StringBuilder builder = new StringBuilder(QQConstants.GET_OPENID_URL);
        builder.append("?access_token=");
        builder.append(getAccessToken());
        return builder;
    }

    @Override
    protected void execut0(
            String responseData) {
        LOGGER.debug("get response data[{}]", responseData);

        OpenIdInfo openIdInfo = null;

        if (responseData.indexOf("callback") != -1) {
            openIdInfo = parseResponseJsData(responseData, OpenIdInfo.class);
        } else {
            Map<String, String> params = URLUtil.paramsParse(responseData);
            openIdInfo = new OpenIdInfo();
            openIdInfo.setOpenId(params.get("openid"));
            openIdInfo.setClientId(params.get("client_id"));
        }

        if (openIdInfo == null || StringUtils.isBlank(openIdInfo.getOpenId())) {
            throw new QQOauth2Exception("openid is null");
        }

        response = new OpenIdResponse();
        response.setData(openIdInfo);
    }

    public void setAccessToken(
            String code) {
        requestParams.put("access_token", code);
    }

    public String getAccessToken() {
        return requestParams.get("access_token");
    }
}
