package net.guerlab.sdk.qq.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OpenIdInfo {

    @JsonProperty("openid")
    private String openId;

    @JsonProperty("client_id")
    private String clientId;

    /**
     * 返回 openId
     *
     * @return openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 设置openId
     *
     * @param openId
     *            openId
     */
    public void setOpenId(
            String openId) {
        this.openId = openId;
    }

    /**
     * 返回 clientId
     *
     * @return clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * 设置clientId
     *
     * @param clientId
     *            clientId
     */
    public void setClientId(
            String clientId) {
        this.clientId = clientId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OpenIdInfo [openId=");
        builder.append(openId);
        builder.append(", clientId=");
        builder.append(clientId);
        builder.append("]");
        return builder.toString();
    }
}
