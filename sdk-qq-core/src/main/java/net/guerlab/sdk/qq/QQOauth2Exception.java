package net.guerlab.sdk.qq;

public class QQOauth2Exception extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public QQOauth2Exception(String message) {
        super(message);
    }

    public QQOauth2Exception(String message, Exception e) {
        super(message, e);
    }
}
