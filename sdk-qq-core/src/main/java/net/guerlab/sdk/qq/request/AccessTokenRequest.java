package net.guerlab.sdk.qq.request;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.qq.QQConstants;
import net.guerlab.sdk.qq.QQOauth2Exception;
import net.guerlab.sdk.qq.response.AccessToken;
import net.guerlab.sdk.qq.response.AccessTokenResponse;
import net.guerlab.sdk.qq.util.URLUtil;

public class AccessTokenRequest extends AbstractRequest<AccessTokenResponse, AccessToken> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessTokenRequest.class);

    @Override
    public StringBuilder createRequestUrl(
            String appId,
            String appKey) {
        StringBuilder builder = new StringBuilder(QQConstants.ACCESS_TOKEN_URL);
        builder.append("?grant_type=authorization_code&client_id=");
        builder.append(appId);
        builder.append("&client_secret=");
        builder.append(appKey);
        return builder;
    }

    @Override
    protected void execut0(
            String responseData) {
        LOGGER.debug("get response data[{}]", responseData);

        AccessToken accessToken = null;
        if (responseData.indexOf("callback") != -1) {
            accessToken = parseResponseJsData(responseData, AccessToken.class);
        } else {
            Map<String, String> params = URLUtil.paramsParse(responseData);

            String accessTokenString = params.get("access_token");
            String refreshToken = params.get("refresh_token");
            long expiresIn = 0;
            try {
                expiresIn = Long.parseLong(params.get("expires_in"));
                accessToken = new AccessToken();
                accessToken.setAccessToken(accessTokenString);
                accessToken.setExpiresIn(expiresIn);
                accessToken.setRefreshToken(refreshToken);
            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }

        if (accessToken == null) {
            throw new QQOauth2Exception("get access_token fail");
        }

        response = new AccessTokenResponse();
        response.setData(accessToken);
    }

    public void setCode(
            String code) {
        requestParams.put("code", code);
    }

    public String getCode() {
        return requestParams.get("code");
    }

    public void setRedirectUri(
            String redirectUri) {
        requestParams.put("redirect_uri", redirectUri);
    }

    public String getRedirectUri() {
        return requestParams.get("redirect_uri");
    }
}
